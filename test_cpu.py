import keyboard
from threading import Thread
import psutil
import time
import csv
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("filename", type=str,
                    help="filename to save to")
parser.add_argument("-t", action="store_true",
                    help="report total cpu usage rather than per cpu usage")
args = parser.parse_args()

exit_loop = False

cpu_usages = []

def display_cpu():
    print("Monitoring CPU usage...")
    psutil.cpu_percent(percpu=(not args.t))
    while True:
        if (exit_loop):
            break
        cpu_usages.append(psutil.cpu_percent(percpu=(not args.t)))
        time.sleep(1)
    print("Writing output csv")
    with open(args.filename, mode='w') as cpu_file:
        csv_writer = csv.writer(cpu_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        #CPUS = ["CPU " + str(i) for i in range(len(cpu_usages))]
        #csv_writer.writerow(CPUS)
        for i in range(len(cpu_usages)):
            if type(cpu_usages[i]) is not list:
                cpu_usage_list = [cpu_usages[i]]
            else:
                cpu_usage_list = cpu_usages[i]
            csv_writer.writerow(cpu_usage_list)


try:
    t = Thread(target=display_cpu,args=())
    t.start()
except:
    print("error occurred")


while True:
    if keyboard.is_pressed("esc") or keyboard.is_pressed("enter"):
        exit_loop = True
        break
print("loop_terminated")
t.join()