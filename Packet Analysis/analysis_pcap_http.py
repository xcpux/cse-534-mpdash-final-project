import dpkt
import copy


class TCP_Packet:
    FIN_FLAG = 1
    SYN_FLAG = 2
    ACK_FLAG = 16
    windowscale = None

    def __init__(self, pkt, timestamp):
        self.checkPacket(pkt,timestamp)

    # Parses a timestamp and a raw packet into a TCP_Packet object, fields are filled in based on
    # the tcp header format.
    def checkPacket(self,bits,timestamp):
        self.type = None
        self.fin = False
        self.syn = False
        self.ack = False
        self.mss = -1
        self.timestamp = timestamp
        # Skip ethernet header
        offset = 14
        internet_header_size = (bits[offset] & 15) * 4
        offset += internet_header_size
        tcp_app = bits[offset:]
        self.source = int.from_bytes(tcp_app[0:2], byteorder="big")
        self.dest = int.from_bytes(tcp_app[2:4], byteorder="big")
        self.seq = int.from_bytes(tcp_app[4:8], byteorder="big")
        flags = tcp_app[13]
        self.ProcessFlags(flags)
        if (self.ack):
            self.acknum = int.from_bytes(tcp_app[8:12], byteorder="big")
        options = tcp_app[20:]
        self.app_data = self.ProcessOptions(options)
        self.windowsize = int.from_bytes(tcp_app[14:16], byteorder="big")
        if (TCP_Packet.windowscale is not None):
            self.windowsize *= TCP_Packet.windowscale
        self.size = len(bits)

    # Processes options portion based on option value.
    def ProcessOptions(self, options):
        currentOption = -1
        offset = 0
        while (currentOption != 0) and (offset < len(options)):
            currentOption = options[offset]
            if (currentOption == 1):
                offset += 1;
            elif(currentOption == 0):
                break
            elif(currentOption == 2):
                self.mss = int.from_bytes(options[offset+2:offset+4],byteorder="big")
                offset += 4;
            elif (currentOption == 3):
                TCP_Packet.windowscale = 2**options[offset+2]
                offset +=3
            elif (currentOption == 4):
                offset+= 2
            elif (currentOption == 8):
                offset += 10
            elif (currentOption == 18):
                offset += 3
            elif (currentOption == 27):
                offset += 8
            else:
                break
        return options[offset:]

    # Processes the flag byte of TCP packet header, only SYN, ACK, and FIN are of value.
    def ProcessFlags(self, flags):
        if (flags & self.FIN_FLAG):
            self.fin = True
        if (flags & self.SYN_FLAG):
            self.syn = True
        if (flags & self.ACK_FLAG):
            self.ack = True

    ##Used to relativize all packets timestamp with relation to the first packet sent.
    def SetTimestamp(self, timedelta):
        self.timestamp -= timedelta

    #Used to relativize sequence number with respec
    def RelativizeSeq(self, seq):
        self.seq -= seq


#Classifies a unique TCP connection with the packets sent by the sender and the acks sent by the receiver.
class TcpConnection:
    def __init__(self, receivePort, port, synack):
        self.port = port
        self.receivePort = receivePort
        self.sentPackets = list()
        self.receivedPackets = list()
        self.sentPackets.append(synack)

#Parses list of TCP_Packet objects and forms TCPConnection objects from them.
def CollectPackets(tcppkts, recieveport):
    rport = recieveport
    connections = []
    for tcppkt in tcppkts:
        if (tcppkt.syn == True and tcppkt.ack == False):
            connections.append(TcpConnection(tcppkt.dest,tcppkt.source,tcppkt))
        else:
            for connection in connections:
                if (tcppkt.source == rport):
                    if (tcppkt.dest == connection.port):
                        connection.receivedPackets.append(tcppkt)
                        break
                elif (tcppkt.dest == rport):
                    if (tcppkt.source == connection.port):
                        connection.sentPackets.append(tcppkt)
    return connections

#Counts Retransmissions based on number of identical seq numbered packets
#Counts total bytes by counting the length of each packet once (dup seqs are only counted once).
def CountRetransmissionsAndTotalSent(transactions):
    retransmissions = 0
    totalsize = 0
    for transac in transactions:
        transac.sentPackets.sort(key=lambda x:x.seq)
        regfactor = transac.sentPackets[0].seq
        lastseen = -1
        inrow = 0
        for packet in transac.sentPackets:
            packet.RelativizeSeq(regfactor)
            if (packet.seq == lastseen):
                inrow += 1
            else:
                totalsize += packet.size
                lastseen = packet.seq
                retransmissions += inrow
                inrow = 0
                #Compensate for 3 way handshake
        retransmissions -= 1
    return retransmissions,totalsize

#Gets ack,seq,receive_window, and average RTTs of a list of TCP transactions.
def GetStatisticsOnTransactions(transactions):
    transactions.sort(key=lambda x:x.sentPackets[0].timestamp)
    copytrans = copy.deepcopy(transactions)
    seqnums = list()
    acknums = list()
    recieve_windows = list()
    RTTs = 0
    numpackets = 0
    for transac in transactions:
        transac.sentPackets.sort(key=lambda x:x.seq)
        transac.receivedPackets.sort(key=lambda x: x.acknum)
        seqnums.append(transac.sentPackets[1].seq)
        acknums.append(transac.sentPackets[1].acknum)
        recieve_windows.append(transac.sentPackets[1].windowsize)
        resrtt, ressize = CalcAverageRTT(transac)
        RTTs += resrtt
        numpackets += ressize
    return seqnums, acknums, recieve_windows, RTTs/numpackets

#Adds up the RTT of each received packet with the sent packet that it corresponds to and divides it based on number of additions.
def CalcAverageRTT(transaction):
    RTT = 0
    groupedR, groupedS = GroupReceivedAndSentPackets(transaction)
    index = 0
    for elem in groupedR:
        firstAck = elem[0]
        while index < len(groupedS):
             #print(firstAck.acknum, groupedS[index][0].seq)
             if firstAck.acknum == groupedS[index][0].seq:
                 lastSent = groupedS[index-1][-1]
                 RTT += firstAck.timestamp - lastSent.timestamp
                 break
             else:
                 index += 1
    return RTT, len(groupedR)

#Drops packets that a
def DropIrrelevantPackets(tcppkts, port):
    results = []
    for pack in tcppkts:
        if pack.source == port or pack.dest == port:
            results.append(pack)
    return results

def GroupReceivedAndSentPackets(transaction):
    results = []
    index = 0
    lastSeen = None
    nextList = None
    while index < len(transaction.receivedPackets):
        if lastSeen is None or lastSeen != transaction.receivedPackets[index].acknum:
            if lastSeen is None:
                nextList = list()
            else:
                nextList.sort(key=lambda x:x.timestamp)
                results.append(nextList)
                nextList = list()
            nextList.append(transaction.receivedPackets[index])
            lastSeen = transaction.receivedPackets[index].acknum
            index += 1
        else:
            nextList.append(transaction.receivedPackets[index])
            index += 1
    results.append(nextList)
    sentresults = []
    index = 0
    lastSeen = None
    nextList = None
    while index < len(transaction.sentPackets):
        if lastSeen is None or lastSeen != transaction.sentPackets[index].seq:
            if lastSeen is None:
                nextList = list()
            else:
                nextList.sort(key=lambda x: x.timestamp)
                sentresults.append(nextList)
                nextList = list()
            nextList.append(transaction.sentPackets[index])
            lastSeen = transaction.sentPackets[index].seq
            index += 1
        else:
            nextList.append(transaction.sentPackets[index])
            index += 1
    sentresults.append(nextList)
    return results, sentresults

def CalculateThroughput(tcppaks, totalsize):
    return totalsize/tcppaks[-1].timestamp

def IdentifyHttpPackets(transaction):
    for elem in transaction.receivedPackets:
        if (len(elem.app_data) != 0):
            elem.type = "response"
    for elem in transaction.sentPackets:
        if (len(elem.app_data) != 0):
            elem.type = "request"

def DisplayHttpPackets(pkts):
    print("Displaying http packs for connections between", transaction.port,"and", transaction.receivePort)
    for pkt in pkts:
        if (pkt.type != None):
            tup = (pkt.source, pkt.dest,pkt.seq,pkt.acknum)
            print(pkt.type, tup)

def GetNumberTransmitted(transactions):
    numpackets = 0
    size = 0
    for transac in transactions:
        for elem in transac.sentPackets:
            numpackets+=1
            size+=elem.size
    return numpackets,size

def FigureOutHTTP(numConnections, ports):
    maxconn = numConnections.index(max(numConnections))
    minconn = numConnections.index(min(numConnections))
    middleconn = 3 - maxconn - minconn
    print(ports[maxconn], "runs on HTTP 1.0")
    print(ports[middleconn], "runs on HTTP 1.1")
    print(ports[minconn], "runs on HTTP 2.0")

f1 = open("http_1080.pcap","rb")
f2 = open("tcp_1081.pcap","rb")
f3 = open("tcp_1082.pcap","rb")
tcp_filter = lambda x:len(x[1]) >= 54
pcap = dpkt.pcap.Reader(f1)
pkts = list(filter(tcp_filter, pcap.readpkts()))
tcppaks1 = [TCP_Packet(pkt[1], pkt[0]) for pkt in pkts]
tcppaks1 = DropIrrelevantPackets(tcppaks1,1080)
tcppaks1.sort(key=lambda x:x.timestamp)
timedelta = tcppaks1[0].timestamp
for pkt in tcppaks1:
    pkt.SetTimestamp(timedelta)
transactions1 = CollectPackets(tcppaks1,1080)
for transaction in transactions1:
    IdentifyHttpPackets(transaction)
DisplayHttpPackets(tcppaks1)
pcap = dpkt.pcap.Reader(f2)
pkts = list(filter(tcp_filter, pcap.readpkts()))
tcppaks2 = [TCP_Packet(pkt[1],pkt[0]) for pkt in pkts]
tcppaks2 = DropIrrelevantPackets(tcppaks2,1081)
tcppaks2.sort(key=lambda x:x.timestamp)
timedelta = tcppaks2[0].timestamp
for pkt in tcppaks2:
    pkt.SetTimestamp(timedelta)
transactions2 = CollectPackets(tcppaks2,1081)
for transaction in transactions2:
    IdentifyHttpPackets(transaction)
DisplayHttpPackets(tcppaks2)
pcap = dpkt.pcap.Reader(f3)
pkts = list(filter(tcp_filter, pcap.readpkts()))
tcppaks3 = [TCP_Packet(pkt[1],pkt[0]) for pkt in pkts]
tcppaks3 = DropIrrelevantPackets(tcppaks3,1082)
tcppaks3.sort(key=lambda x:x.timestamp)
timedelta = tcppaks3[0].timestamp
for pkt in tcppaks3:
    pkt.SetTimestamp(timedelta)
transactions3 = CollectPackets(tcppaks3,1082)
for transaction in transactions3:
    IdentifyHttpPackets(transaction)
DisplayHttpPackets(tcppaks3)
print("1080 Elapsed time",tcppaks1[-1].timestamp)
print("1081 Elapsed time",tcppaks2[-1].timestamp)
print("1082 Elapsed time",tcppaks3[-1].timestamp)
print("# of TCP connections opened 1080:",len(transactions1))
print("# of TCP connections opened 1081:",len(transactions2))
print("# of TCP connections opened 1082:",len(transactions3))
FigureOutHTTP([len(transactions1), len(transactions2),len(transactions3)],[1080,1081,1082])
packets1, size1 = GetNumberTransmitted(transactions1)
packets2, size2 = GetNumberTransmitted(transactions2)
packets3, size3 = GetNumberTransmitted(transactions3)
print("Number of packets transmitted 1080:" ,  packets1, size1,"bytes")
print("Number of packets transmitted 1081:" ,  packets2, size2,"bytes")
print("Number of packets transmitted 1082:" ,  packets3, size3,"bytes")
f1.close()
f2.close()
f3.close()