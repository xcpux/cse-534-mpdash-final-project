import dpkt
from construct import *


PCAP_FILE = '../rsrc/assignment2.pcap'


class EthernetPacketBuilder:
    """
    A wrapper class to contain Ethernet packet data
    """

    def __init__(self, bin_data):
        """
        Will create instance with appropriate fields filled in.
        :param bin_data: byte array containing packet data.
        """
        self.bin_data = bin_data
        # Struct object to use for building instance fields
        self.header_struct = Struct(
            "dest_mac" / CString("utf8"),
            "src_mac" / CString("utf8"),
            "type" / Int16ub
        )
        self.payload = bin_data[14:] # packet payload
        self.raw_header = self.header_struct.build(EthernetPacketBuilder.parseHeader(bin_data)) # parse data
        self.header = self.header_struct.parse(self.raw_header) # builds class fields

    @staticmethod
    def parseMac(address):
        # parse byte string for mac address into human readable format
        return ("%.2x:%.2x:%.2x:%.2x:%.2x:%.2x" % tuple(ord(chr(x)) for x in address)).decode('utf8')

    @staticmethod
    def parseHeader(packet):
        # Decode packet header
        data = dict()
        data["dest_mac"] = EthernetPacketBuilder.parseMac(packet[0:6]) # bytes 0-5 are dest mac
        data["src_mac"] = EthernetPacketBuilder.parseMac(packet[6:12]) # bytes 6-11 are src mac
        data["type"] = (int(packet[12]) << 8) | int(packet[13]) # bytes 12-13 are payload packet type
        return data


class IPv4PacketBuilder:
    def __init__(self, bin_data):
        self.bin_data = bin_data
        self.header_struct = Struct(
            "version" / Int8ub,
            "header_length" / Int8ub,
            "dscp" / Int8ub,
            "ecn" / Int8ub,
            "total_length" / Int16ub,
            "identification" / Int16ub,
            "flags" / Int8ub,
            "offset" / Int24ub,
            "ttl" / Int8ub,
            "protocol" / Int8ub,
            "checksum" / Int16ub,
            "src_ip" / Int32ub,
            "dest_ip" / Int32ub
        )
        self.raw_header = self.header_struct.build(IPv4PacketBuilder.parseHeader(bin_data))
        self.header = self.header_struct.parse(self.raw_header)
        self.payload = bin_data[20:]

    @staticmethod
    def parseHeader(packet):
        data = dict()
        data["version"] = int(packet[0]) >> 4
        data["header_length"] = (int(packet[0]) & 0xF) << 2
        data["dscp"] = (int(packet[1]) & 0x3F)
        data["ecn"] = (int(packet[1]) >> 6) & 0x2
        data["total_length"] = (int(packet[2]) << 8) | (int(packet[3]))
        data["identification"] = (int(packet[4]) << 8) | (int(packet[5]))
        data["flags"] = (int(packet[6]) >> 5) & 0x7
        data["offset"] = ((int(packet[6]) & 0x1F) | int(packet[7]))
        data["ttl"] = int(packet[8])
        data["protocol"] = int(packet[9])
        data["checksum"] = (int(packet[10]) << 8) | (int(packet[11]))
        data["src_ip"] = (int(packet[12]) << 24) | (int(packet[13]) << 16) | (int(packet[14]) << 8) | (int(packet[15]))
        data["dest_ip"] = (int(packet[16]) << 24) | (int(packet[17]) << 16) | (int(packet[18]) << 8) | (int(packet[19]))

        return data


class TCPPacketBuilder:
    def __init__(self, bin_data):
        tcp_header = Struct(
            "src" / Int16ub,
            "dest" / Int16ub,
            "seq" / Int32ub,
            "ack" / Int32ub,
            "dataoff" / Int8ub,
            "flags" / Int16ub,
            "winsize" / Int16ub,
            "checksum" / Int16ub,
            "urgent" / Int16ub,
            "options" / GreedyBytes,
        )

        self.bin_data = bin_data
        self.header_data = TCPPacketBuilder.parseHeader(bin_data)
        self.header = tcp_header.parse(tcp_header.build(self.header_data))
        self.payload = bin_data[self.header.dataoff << 2:]
        self.timestamp = -1

    @staticmethod
    def parseHeader(header):
        data = dict()
        data["src"] = (int(header[0]) << 8) | (int(header[1]))
        data["dest"] = (int(header[2]) << 8) | (int(header[3]))
        data["seq"] = (int(header[4]) << 24) | (int(header[5]) << 16) | (int(header[6]) << 8) | (int(header[7]))
        data["ack"] = (int(header[8]) << 24) | (int(header[9]) << 16) | (int(header[10]) << 8) | (int(header[11]))
        temp1, temp2 = int(header[12]), int(header[13])
        data["dataoff"] = (temp1 >> 4) & 0xF
        data["flags"] = ((temp1 & 1) << 8) | temp2
        data["winsize"] = (int(header[14]) << 8) | (int(header[15]))
        data["checksum"] = (int(header[16]) << 8) | (int(header[17]))
        data["urgent"] = (int(header[18]) << 8) | (int(header[19]))
        header_length = data["dataoff"] << 2
        option_offset = 20
        if header_length - option_offset > 0:
            data["options"] = header[option_offset:header_length]
        else:
            data["options"] = b''
        return data


def countTCPFlows(pcap_reader):
    flowCount = 0
    tcpFlows = dict()
    while True:
        try:
            ts, buff = pcap_reader.__next__()
            rawPacket = bytearray(buff)
            ethPacket = EthernetPacketBuilder(rawPacket)
            ipv4Packet = IPv4PacketBuilder(ethPacket.payload)
            tcpPacket = TCPPacketBuilder(ipv4Packet.payload)
            tcpPacket.timestamp = ts
            ipSource = ipv4Packet.header.src_ip
            ipDest = ipv4Packet.header.dest_ip
            tcpSource = tcpPacket.header.src
            tcpDest = tcpPacket.header.dest
            tcpHeader = tcpPacket.header
            if tcpHeader.flags == 0x12:
                flow = (ipSource, tcpSource, ipDest, tcpDest)
                if flow not in tcpFlows or tcpFlows[flow] != tcpHeader.ack:
                    tcpFlows[flow] = tcpHeader.ack
                    flowCount += 1
        except StopIteration:
            break
        except StandardError:
            continue
    return flowCount


def getTCPFlows(pcap_reader):
    tcpFlows = dict()
    while True:
        try:
            ts, buff = pcap_reader.__next__()
            rawPacket = bytearray(buff)
            ethPacket = EthernetPacketBuilder(rawPacket)
            ipv4Packet = IPv4PacketBuilder(ethPacket.payload)
            tcpPacket = TCPPacketBuilder(ipv4Packet.payload)
            tcpPacket.timestamp = ts
            ipSource = ipv4Packet.header.src_ip
            ipDest = ipv4Packet.header.dest_ip
            tcpSource = tcpPacket.header.src
            tcpDest = tcpPacket.header.dest
            flow = (ipSource, tcpSource, ipDest, tcpDest)
            if flow in tcpFlows:
                tcpFlows[flow].append(tcpPacket)
            elif flow not in tcpFlows and (ipDest, tcpDest, ipSource, tcpSource) in tcpFlows:
                tcpFlows[(ipDest, tcpDest, ipSource, tcpSource)].append(tcpPacket)
            else:
                tcpFlows[flow] = [tcpPacket]
        except StopIteration:
            break
        except StandardError:
            continue
    return tcpFlows


def analyzeTCPFlow(tcpFlows, transactionCount=-1):
    flowCount = 1
    tcpFlowAnalysis = dict()
    for flow, packetList in tcpFlows.iteritems():
        syn = False
        synAck = False
        ack = False
        dataPacketCount = 0
        transactions = dict()
        transactionsCompleted = []
        for packet in packetList:
            packetHeader = packet.header
            if syn and synAck and ack:
                if transactionCount < 0 or len(transactionsCompleted) < transactionCount:
                    if packetHeader.flags & 0x10:
                        if packetHeader.ack in transactions:
                            transactions[packetHeader.ack].append(packet)
                            transactionsCompleted.append(transactions[packetHeader.ack])
                        else:
                            transactions[packetHeader.seq] = [packet]
                else:
                    print 'Transactions:'
                    for transList in transactionsCompleted:
                        for pkt in transList:
                            print pkt.timestamp, pkt.header_data
                        print
                    print
                    break
                dataPacketCount += 1
            if packetHeader.flags == 0x2:
                syn = True
                synAck = False
                ack = False
            elif packetHeader.flags == 0x12:
                synAck = True
                ack = False
            elif packetHeader.flags & 0x10:
                ack = True
        flowCount += 1
        tcpFlowAnalysis[flow] = transactions
        if transactionCount < 0:
            print
    return tcpFlowAnalysis

def calculateThroughputEstimate(analyzedTCPFlows):
    throughputs = dict()
    flowNum = 1
    for flow, transactions in analyzedTCPFlows.iteritems():
        notDroppedPackets = 0
        minTimeStamp = float('inf')
        maxTimestamp = float('-inf')
        print "Flow", flowNum, " throughput =",
        for seq, packetList in transactions.iteritems():
            maxTimestamp = max(maxTimestamp, max(packetList, key=lambda x: x.timestamp).timestamp)
            minTimeStamp = min(minTimeStamp, min(packetList, key=lambda x: x.timestamp).timestamp)
            if len(packetList) == 2:
                notDroppedPackets += len(packetList[0].bin_data)
        throughputs[flow] = float(notDroppedPackets) / float(maxTimestamp - minTimeStamp)
        print throughputs[flow], 'bytes/sec\n'
        flowNum += 1
    return throughputs

def calculateLossRate(analyzedTCPFlows):
    tcpLossRates = dict()
    flowNum = 1
    for flow, transactions in analyzedTCPFlows.iteritems():
        lossNum = 0
        print "Flow", flowNum, " loss rate =",
        for seq, packetList in transactions.iteritems():
            if len(packetList) == 1:
                lossNum += 1
        lossRate = float(lossNum)/float(len(transactions))
        tcpLossRates[flow] = lossRate
        print float(lossNum)/float(len(transactions)), '\n'
        flowNum += 1
    return tcpLossRates


def calculateAverageRTT(analyzedTCPFlows):
    averageRTTs = dict()
    flowNum = 1
    for flow, transactions in analyzedTCPFlows.iteritems():
        numTransactions = 0
        averageRTT = 0.0
        print "Flow", flowNum, " Average RTT =",
        for seq, packetList in transactions.iteritems():
            if len(packetList) == 2 and packetList[0].timestamp > 0 and packetList[1].timestamp > 0:
                averageRTT += packetList[1].timestamp - packetList[0].timestamp
                numTransactions += 1
        if numTransactions > 0:
            averageRTT /= float(numTransactions)
            averageRTT *= 1e3
        averageRTTs[flow] = averageRTT
        print averageRTT, 'ms\n'
        flowNum += 1
    return averageRTTs

if __name__ == '__main__':
    f = open(PCAP_FILE, 'rb')
    pcap = dpkt.pcap.Reader(f)
    print "Parsing TCP Flows ... "
    tcp_flows = getTCPFlows(pcap)
    print "Finished parsing TCP Flows.\n"
    print "Analyzing TCP Flows ..."
    tcp_analysis = analyzeTCPFlow(tcp_flows)
    print "Analysis complete.\n"
    print "Calculating Loss Rate ..."
    calculateThroughputEstimate(tcp_analysis)
    calculateLossRate(tcp_analysis)
    calculateAverageRTT(tcp_analysis)
