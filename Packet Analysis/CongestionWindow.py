import dpkt

gap = 0.08 * 0.6

class TCP_Packet:
    FIN_FLAG = 1
    SYN_FLAG = 2
    ACK_FLAG = 16
    windowscale = None

    def __init__(self, pkt, timestamp):
        self.checkPacket(pkt,timestamp)

    def checkPacket(self,bits,timestamp):
        self.fin = False
        self.syn = False
        self.ack = False
        self.mss = -1
        self.timestamp = timestamp
        # Skip ethernet header
        offset = 14
        internet_header_size = (bits[offset] & 15) * 4
        offset += internet_header_size
        tcp_app = bits[offset:]
        self.source = int.from_bytes(tcp_app[0:2], byteorder="big")
        self.dest = int.from_bytes(tcp_app[2:4], byteorder="big")
        self.seq = int.from_bytes(tcp_app[4:8], byteorder="big")
        flags = tcp_app[13]
        self.ProcessFlags(flags)
        if (self.ack):
            self.acknum = int.from_bytes(tcp_app[8:12], byteorder="big")
        options = tcp_app[20:]
        self.app_data = self.ProcessOptions(options)
        self.windowsize = int.from_bytes(tcp_app[14:16], byteorder="big") * TCP_Packet.windowscale
        self.size = len(bits)

    def ProcessOptions(self, options):
        currentOption = -1
        offset = 0
        while (currentOption != 0) and (offset < len(options)):
            currentOption = options[offset]
            if (currentOption == 1):
                offset += 1;
            elif(currentOption == 0):
                break
            elif(currentOption == 2):
                self.mss = int.from_bytes(options[offset+2:offset+4],byteorder="big")
                offset += 4;
            elif (currentOption == 3):
                TCP_Packet.windowscale = 2**options[offset+2]
                offset +=3
            elif (currentOption == 4):
                offset+= 2
            elif (currentOption == 8):
                offset += 10
            elif (currentOption == 18):
                offset += 3
            elif (currentOption == 27):
                offset += 8
            else:
                break
        return options[offset+1:]



    def ProcessFlags(self, flags):
        if (flags & self.FIN_FLAG):
            self.fin = True
        if (flags & self.SYN_FLAG):
            self.syn = True
        if (flags & self.ACK_FLAG):
            self.ack = True

    def SetTimestamp(self, timedelta):
        self.timestamp -= timedelta

    def RelativizeSeq(self, seq):
        self.seq -= seq

class TcpConnection:
    def __init__(self, receivePort, port, synack):
        self.port = port
        self.receivePort = receivePort
        self.sentPackets = list()
        self.receivedPackets = list()
        self.sentPackets.append(synack)

def CollectPackets(tcppkts, recieveport):
    rport = recieveport
    connections = []
    for tcppkt in tcppkts:
        if (tcppkt.syn == True and tcppkt.ack == False):
            connections.append(TcpConnection(tcppkt.dest,tcppkt.source,tcppkt))
        else:
            for connection in connections:
                if (tcppkt.source == rport):
                    if (tcppkt.dest == connection.port):
                        connection.receivedPackets.append(tcppkt)
                        break
                elif (tcppkt.dest == rport):
                    if (tcppkt.source == connection.port):
                        connection.sentPackets.append(tcppkt)
    return connections

def GetCongestionWindow(transaction):
    results = list()
    curresults = list()
    lasttime = 0
    regfactor = transaction.sentPackets[1].timestamp
    #Ignore the first packet since the three way handshake will always send one packet and wait for response.
    for packet in transaction.sentPackets[1:]:
        packet.SetTimestamp(regfactor)
        if (packet.timestamp) < lasttime + gap:
            curresults.append(packet)
        else:
            results.append(curresults)
            curresults = list()
            curresults.append(packet)
            lasttime = packet.timestamp
    return results

def CountRetransmissions(window):
    total = 0
    for elem in window:
        if (elem <= 2):
            total += 1
    return total

def GroupReceivedPackets(transaction):
    results = []
    index = 0
    lastSeen = None
    nextList = None
    while index < len(transaction.receivedPackets):
        if lastSeen is None or lastSeen != transaction.receivedPackets[index].acknum:
            if lastSeen is None:
                nextList = list()
            else:
                nextList.sort(key=lambda x:x.timestamp)
                results.append(nextList)
                nextList = list()
            nextList.append(transaction.receivedPackets[index])
            lastSeen = transaction.receivedPackets[index].acknum
            index += 1
        else:
            nextList.append(transaction.receivedPackets[index])
            index += 1
    results.append(nextList)
    return results



f = open("assignment2.pcap","rb")
pcap = dpkt.pcap.Reader(f)
pkts = pcap.readpkts()
tcppaks = [TCP_Packet(pkt[1], pkt[0]) for pkt in pkts]
timedelta = tcppaks[0].timestamp
for pkt in tcppaks:
    pkt.SetTimestamp(timedelta)
sortbytimestamp = lambda x: x.timestamp
tcppaks.sort(key=sortbytimestamp)
transactions = CollectPackets(tcppaks,80)
retransmissions = 0
tripacks = 0
for transac in transactions:
    #transac.sentPackets.sort(key=lambda x:x.seq)
    transac.receivedPackets.sort(key=lambda x: x.acknum)
    congestpackets = GetCongestionWindow(transac)
    rec = GroupReceivedPackets(transac)
    dupacks  = [len(elem) for elem in rec]
    congestwindow = [len(elem) for elem in congestpackets]
    print(congestwindow)
    for elem in dupacks:
        if elem >= 4:
            tripacks += 1
    retransmissions += CountRetransmissions(congestwindow)
print("Retransmissions ", retransmissions)
print("Trip acks ", tripacks)
f.close()