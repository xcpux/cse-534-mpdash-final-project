#include <net/inet_common.h>
#include <net/inet6_hashtables.h>
#include <net/ipv6.h>
#include <net/ip6_checksum.h>
#include <net/mptcp.h>
#include <net/mpdash.h>
#include <net/mptcp_v4.h>
#include <net/mptcp.h>
#include <net/sock.h>
#include <net/tcp.h>
#include <net/tcp_states.h>
#include <net/transp_v6.h>
#include <net/xfrm.h>

#include <linux/cryptohash.h>
#include <linux/kconfig.h>
#include <linux/module.h>
#include <linux/netpoll.h>
#include <linux/proc_fs.h>
#include <linux/list.h>
#include <linux/jhash.h>
#include <linux/tcp.h>
#include <linux/net.h>
#include <linux/in.h>
#include <linux/random.h>
#include <linux/inetdevice.h>
#include <linux/workqueue.h>
#include <linux/atomic.h>
#include <linux/sysctl.h>
#include <linux/time.h>

int sysctl_mpdash_enabled __read_mostly = 1;
bool mpdash_init_failed __read_mostly;
bool mpdash_active;
bool cellular_enabled;

static struct ctl_table mpdash_table[] = {
	{
		.procname = "mpdash_enabled",
		.data = &sysctl_mpdash_enabled,
		.maxlen = sizeof(int),
		.mode = 0644,
		.proc_handler = &proc_dointvec
	},
	{ }
};

void mpdash_enable_sock(struct sock *sk) {
	if(alpha_num > alpha_denum || hw_alpha_num > hw_alpha_denum 
		|| hw_beta_num > hw_beta_denum) {
		return;
	}
	struct tcp_sock *tcpsk = tcp_sk(sk);
	tcpsk->dash_ctrl_state.segment_set = false;
	tcpsk->dash_ctrl_state.deadline_set = false;
	mpdash_active = true;
	if (!sock_flag(sk, SOCK_MPDASH)) {
		sock_set_flag(sk, SOCK_MPDASH);
	}
}

void mpdash_disable_sock(struct sock *sk) {
	int err = 0;
	struct tcp_sock *tcpsk = tcp_sk(sk);
	if (sock_flag(sk, SOCK_MPDASH)) {
		sock_reset_flag(sk, SOCK_MPDASH);
	}
	//Remember to reset the segment_set and deadline_set variables.
	tcpsk->dash_ctrl_state.segment_set = false;
	tcpsk->dash_ctrl_state.deadline_set = false;
	/* Switch over to mptcp scheduler */
	lock_sock(sk);
	err = mptcp_set_scheduler(sk, "default");
	release_sock(sk);
	mpdash_active = false;
}

void mpdash_set_segment_size(struct tcp_sock *sk, __u64 segment_size) {
	int err = 0;
	sk->dash_segment_size = segment_size;
	sk->dash_ctrl_state.segment_set = true;
	if(sk->dash_ctrl_state.deadline_set) {
		cellular_enabled = false;
		mpdash_active = true;
		//Switch to mpdash_scheduler when both params are set.
		lock_sock((struct sock*)sk);
		err = mptcp_set_scheduler((struct sock*)sk, "mpdash");
		release_sock((struct sock*)sk);
	}
}

void mpdash_set_deadline(struct tcp_sock *sk, __u64 deadline) {
	int err = 0;
	sk->dash_deadline = deadline;
	sk->dash_ctrl_state.deadline_set = true;
	if(sk->dash_ctrl_state.segment_set) {
		cellular_enabled = false;
		mpdash_active = true;
		//Switch to mpdash_scheduler when both params are set.
		lock_sock((struct sock*)sk);
		err = mptcp_set_scheduler((struct sock*)sk, "mpdash");
		release_sock((struct sock*)sk);
	}
}

/* General initialization of MPDASH */
void __init mpdash_init(void) {
	struct ctl_table_header *mpdash_sysctl;
	mpdash_sysctl = register_net_sysctl(&init_net, "net/mpdash", mpdash_table);

	if (!mpdash_sysctl)
		goto register_sysctl_failed;

	mpdash_init_failed = false;

	return;

	register_sysctl_failed:
		mpdash_init_failed = true;
}

struct mpdash_info get_dash_info(struct sock *sk, char __user *optval, unsigned int optlen){
	struct tcp_sock *tcpsock = tcp_sk(sk);
	struct mptcp_info *info = mptcp_get_info2(sk, optval, optlen);
	struct mpdash_info dash_info;
	dash_info.conn_info = info;
	dash_info.dash_segment_size = tcpsock->dash_segment_size;
	dash_info.dash_deadline = tcpsock->dash_deadline;
	return dash_info;
}

void mpdash_set_priority_sock(struct sock *meta_sk, __be32 ip_addr) {
	struct mptcp_cb *mpcb = tcp_sk(meta_sk)->mpcb;
	struct tcp_sock *tp_it;
	struct tcp_sock *priority;
	mptcp_for_each_tp(mpcb, tp_it) {
		 if (tp_it->inet_conn.icsk_inet.sk.__sk_common.skc_daddr == ip_addr)
		 {
			 priority = tp_it;
			 break;
		 }
	}
	mptcp_for_each_tp(mpcb, tp_it) {
		 tp_it -> dash_ctrl_state.user_bias = priority;
	}
}

