#include <net/weights.h>

void HoltWinters(u64* forecast, u64* level, u64* trend, u64 observation,
 u64 alpha_num, u64 alpha_den, u64 beta_num, u64 beta_den){
    if (alpha_num < 1 || alpha_den < 1 || beta_num < 1 || beta_den < 1
    || alpha_num >= alpha_den || beta_num >= beta_den) {
        return;
    }
    if (*level) {
        //initialize level if needed
        *level = observation;
        *forecast = observation;
        return;
    }
    //Do forecast
    u64 last_level = *level;
    u64 last_trend = *trend;
    u64 comp_alpha_num = alpha_den - alpha_num;
    u64 comp_beta_num = beta_den - beta_num;
    *level = (alpha_num * observation)/alpha_den +  ((comp_alpha_num)*(last_level + last_trend))/alpha_den;
    *trend = (beta_num * (*level - last_level))/beta_den + ((comp_beta_num)* last_trend)/beta_den;
    *forecast = *level + *trend;
}

void EPWA(u64* forecast, u64 alpha_num, u64 alpha_den, u64 observation) {
    if (alpha_num < 1 || alpha_den < 1 || alpha_num >= alpha_den) {
        *forecast = -1;
        return;
    }
    if (*forecast == 0) {
        *forecast = observation;
        return;
    }
    u64 last_forecast = *forecast;
    u64 alpha_comp = alpha_den - alpha_num;
    *forecast = (alpha_num * observation)/alpha_den + ((alpha_comp) * last_forecast)/alpha_den;
}