#include <linux/types.h>

void HoltWinters(u64* forecast, u64* level, u64* trend, u64 observation,
 u64 alpha_num, u64 alpha_den, u64 beta_num, u64 beta_den);

void EPWA(u64* forecast, u64 alpha_num, u64 alpha_den, u64 observation);