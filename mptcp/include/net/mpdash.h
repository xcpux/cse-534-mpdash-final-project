#ifndef _MPDASH_H
#define _MPDASH_H

#include <linux/inetdevice.h>
#include <linux/ipv6.h>
#include <linux/list.h>
#include <linux/net.h>
#include <linux/netpoll.h>
#include <linux/siphash.h>
#include <linux/skbuff.h>
#include <linux/socket.h>
#include <linux/tcp.h>
#include <linux/kernel.h>

#include <asm/byteorder.h>
#include <asm/unaligned.h>
#include <crypto/hash.h>
#include <net/tcp.h>
#include <net/sock.h>

void __init mpdash_init(void);

extern int sysctl_mpdash_enabled;
extern bool mpdash_init_failed;
extern bool mpdash_active;
extern bool cellular_enabled;
extern bool cellular_enabled_server;
extern __be32 priority_ip;
extern __u64 alpha_num;
extern __u64 alpha_denum;
extern __u64 hw_alpha_num;
extern __u64 hw_alpha_denum;
extern __u64 hw_beta_num;
extern __u64 hw_beta_denum;
void mpdash_enable_sock(struct sock *sk);
void mpdash_disable_sock(struct sock *sk);
void mpdash_set_segment_size(struct tcp_sock *sk, __u64 segment_size);
void mpdash_set_deadline(struct tcp_sock *sk, __u64 deadline);
struct mpdash_info get_dash_info(struct sock *sk, char __user *optval, unsigned int optlen);
void mpdash_set_priority_sock(struct sock *meta_sk, __be32 ip_addr);
void mpdash_set_alpha_num(struct sock *sk, __u64 num);
void mpdash_set_alpha_denum(struct sock *sk, __u64 denum);
void mpdash_set_hw_alpha_num(struct sock *sk, __u64 num);
void mpdash_set_hw_alpha_denum(struct sock *sk, __u64 denum);
void mpdash_set_hw_beta_num(struct sock *sk, __u64 num);
void mpdash_set_hw_beta_denum(struct sock *sk, __u64 denum);

// #ifdef CONFIG_MPDASH
// TODO: add inline stubs
// #endif /* CONFIG_MPDASH */

#endif /* _MPDASH_H*/