import matplotlib.pyplot as plt
import argparse
import random
import itertools
import csv

colors = ["b", "g", "r", "k", "y", "c", "m"]
styles = ["--", "-","--", "-.", "-", ":"]
markers = ["o", "*", "+", "D", "^"]
colors_choices = list(range(6))
styles_choices = list(range(5))
markers_choices = list(range(5))

def GeneralPlot(inputs, outputs, permute,legend=None,xaxis=None,yaxis=None,title=None):
    lines = []
    plots = []
    if (not len(inputs) == len(outputs)):
        raise Exception("Input size doesn't match output size")
    if (type(outputs[0]) is not list):
        lines = [inputs, outputs]
    else:
        for i in range(len(outputs[0])):
            line_outputs = [outputs[j][i] for j in range(len(outputs))]
            lines.append([inputs, line_outputs])
    if (permute):
        #Adds randomness to which permuation to use.
        r6 = itertools.permutations(range(7))
        r5 = itertools.permutations(range(5))
        for i in range (random.randint(0,20)):
            next(r6)
        colors_choices = next(r6)   
        for i in range (random.randint(0,20)):
            next(r5)
        styles_choices = next(r5)
        for i in range (random.randint(0,20)):
            next(r5)
        markers_choices = next(r5)
    for i in range(len(lines)):
        if (permute):
            linestyle = colors[colors_choices[i%len(colors_choices)]] + styles[styles_choices[i%len(styles_choices)]] + markers[markers_choices[i%len(markers_choices)]]
        else:
            linestyle = colors[i%len(colors_choices)] + styles[i%len(styles_choices)] + markers[i%len(markers_choices)]
        plot, = plt.plot(lines[i][0],lines[i][1], linestyle)
        plots.append(plot)
        #print("plotted something")
    if (yaxis):
        plt.ylabel(yaxis)
    if (xaxis):
        plt.xlabel(xaxis)
    if (title):
        plt.title(title)
    if (legend):
        plt.legend(plots,legend)
    plt.show()

def PlotFromCSV(filename,permute,xaxis=None,yaxis=None,title=None,legend=None):
    with open(filename,"r") as csv_file:
        reader = csv.reader(csv_file)
        outputs = []
        for row in reader:
            float_row = [float(item) for item in row]
            outputs.append(float_row)
    GeneralPlot(list(range(1,len(outputs)+1)),outputs,permute,xaxis=xaxis,yaxis=yaxis,title=title,legend=legend)

PlotFromCSV("/home/ubuntu/Downloads/cpus.csv", True,legend=["l1", "l2","l3","l4","l5","l6"])
#plt.plot([1,2,3],[4,5,6])
plt.show()